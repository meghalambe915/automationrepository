package Main.Java;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
//Adding One comment
//Add second Comment
//Add third Comment
class Result {
int a;
int bc;
int c;
    /*
     * Complete the 'diagonalDifference' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY arr as parameter.
     */

    public static int diagonalDifference(List<List<Integer>> arr) {
		return 0;
    // Write your code here

    }

}

public class Diagonal {
    public static void main(String[] args) throws IOException {
      
        ArrayList<Integer> ldpFrrStreams = new ArrayList<Integer>();
        ldpFrrStreams.add(1);
        ldpFrrStreams.add(2);
        ldpFrrStreams.add(3);
        ldpFrrStreams.add(4);
        ldpFrrStreams.add(5);
        ldpFrrStreams.add(6);
        ArrayList<Integer> ldpFrrStreams2 = new ArrayList<Integer>();
        ldpFrrStreams2.add(3);
        ldpFrrStreams2.add(4);
        ldpFrrStreams.removeAll(ldpFrrStreams2);
        ldpFrrStreams.addAll(ldpFrrStreams2);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> arr = new ArrayList<>();

        IntStream.range(0, n).forEach(i -> {
            try {
                arr.add(
                    Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        int result = Result.diagonalDifference(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
